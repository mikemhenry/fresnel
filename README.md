# Fresnel

Fresnel is a python library that ray traces publication quality images in real time.

Read the [tutorial and reference documentation on readthedocs](<https://fresnel.readthedocs.io/>).

## Samples

Here are a few samples of what **fresnel** can do:

![spheres](doc/sphere.png)
Generated by: [sphere.py](doc/sphere.py>).

![cuboids](doc/cuboid.png)
Generated by: [cuboid.py](doc/cuboid.py).

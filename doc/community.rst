User community
==============

fresnel-users mailing list
--------------------------

Subscribe to the `fresnel-users <https://groups.google.com/d/forum/fresnel-users>`_ mailing list to recieve release announcements,
post questions for advice on using the software, and discuss potential new features.

Issue tracker
-------------

File bug reports on `frenel's issue tracker <https://bitbucket.org/glotzer/fresnel/issues?status=new&status=open>`_.

Contribute
----------

**fresnel** is an open source project. Contributions are accepted via pull request to `fresnel's bitbucket repository <https://bitbucket.org/glotzer/fresnel>`_.
Please review ``CONTRIBUTING.MD`` in the repository before starting development. You are encouraged to discuss your proposed contribution with the
**fresnel** user and developer community who can help you design your contribution to fit smoothly into the existing ecosystem.

